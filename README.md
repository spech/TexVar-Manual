# DESCRIPTION
This is the manual for the LuaLaTeX package TexVar.

Gitlab [gitlab.com/Specht08/TexVar](https://gitlab.com/spech/TexVar)

# LICENSE
TexVar is a free software distributed under the terms of the MIT license.

# DEVELOPER
Sebastian Pech
